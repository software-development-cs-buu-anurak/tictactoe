package com.anurak;
/**
 * 
 * Main Application:
 * TicTacToe Program
 * 
 * 63160015 Anurak Yutthanawa
 * B.Sc. Computer Science, Faculty of Informatics
 * Burapha University
 * 
 * 1st projects: TicTacToe
 * Software Development
 * 
 * Create on MAVEN package in Visual Studio Code
 * 
 */

public class App {

    public static void main(String[] args) {
        Table table = new Table();
        Game gameController = new Game(table);
        gameController.start();
    }
}
