package com.anurak;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Game {

    static private Scanner sc = new Scanner(System.in);

    Agent o = new Agent("O");
    Agent x = new Agent("X");
    Table table = new Table();

    Game(Table table) {
        this.table = table;
    }

    public void start() {
        System.out.println("Welcome to OX Game");
        System.out.println(table);
        for (int i = 0; i < 9; i++) {
            i = turnStart(i);
            System.out.println(table);
        }
        System.out.println(">>>Draw<<<");
    }

    private int turnStart(int iter) {
        try {
            turnOperation(iter);
        } catch (IllegalStateException e) {
            System.err.println(e + " Try again [Select unsed location]");
            return --iter;
        } catch (IllegalArgumentException e) {
            System.err.println(e + " Try again [Use number in {1, 2 ,3}]");
            return --iter;
        } catch (InputMismatchException e) {
            System.err.println(e + " Try again [Number only]");
            sc.next();
            return --iter;
        }
        return iter;
    }

    private void turnOperation(int iter) {
        if (checkTurnCondition(iter)) {
            if (turn(o)) {
                oWinPrint();
                System.exit(1);
            }
        } else {
            if (turn(x)) {
                xWinPrint();
                System.exit(1);
            }
        }
    }

    static private boolean checkTurnCondition(int iter) {
        return iter % 2 == 0;
    }

    private void xWinPrint() {
        System.out.println(table);
        System.out.println(">>>X Win<<<");
    }

    private void oWinPrint() {
        System.out.println(table);
        System.out.println(">>>O Win<<<");
    }

    private Boolean turn(Agent agent) {
        System.out.printf("Turn %s\n", agent.toString());
        System.out.println("Please input row, col:");
        int row = sc.nextInt();
        int column = sc.nextInt();
        return table.set(agent, row, column);
    }
}
